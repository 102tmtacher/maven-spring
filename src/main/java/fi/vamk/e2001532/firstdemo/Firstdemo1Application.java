package fi.vamk.e2001532.firstdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Firstdemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(Firstdemo1Application.class, args);
	}

}
