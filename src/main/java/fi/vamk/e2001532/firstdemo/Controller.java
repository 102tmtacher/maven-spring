package fi.vamk.e2001532.firstdemo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@RequestMapping("/test")
    public String test() {
        return "{\"id\":1}";
    }

}
